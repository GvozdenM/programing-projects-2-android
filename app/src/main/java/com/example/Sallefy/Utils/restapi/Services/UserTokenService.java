package com.example.Sallefy.Utils.restapi.Services;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

import com.example.Sallefy.Login.Model.LoginUserToken;
import com.example.Sallefy.Login.Model.UserLogin;


public interface UserTokenService {
    @POST("authenticate")
    Call<LoginUserToken> loginUser(@Body UserLogin login);
}
