package com.example.Sallefy.Utils;


public abstract class AbstractPromise {
    AcceptCallback onSuccess;
    RejectCallback onFail;

    public AbstractPromise() {}

    AbstractPromise(
            AcceptCallback onSuccess,
            RejectCallback onFail) {
        this.onFail = onFail;
        this.onSuccess = onSuccess;
    }

    public abstract void accept();

    public abstract void reject();
}
