package com.example.Sallefy.Utils;



@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
public class ParameterizedPromise<S, R> {
    private AcceptConsumerCallback<S> onSuccess;
    private RejectConsumerCallback<R> onFail;

    public void accept(S data) {
        this.onSuccess.accept(data);
    }

    public void reject(R data) {
        this.onFail.reject(data);
    }
}
