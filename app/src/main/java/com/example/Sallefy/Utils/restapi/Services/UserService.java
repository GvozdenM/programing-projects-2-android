package com.example.Sallefy.Utils.restapi.Services;

import com.example.Sallefy.Login.Model.KeyAndPass;
import com.example.Sallefy.Login.Model.User;
import com.example.Sallefy.Login.Model.UserRegister;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UserService {
    @GET("users/{login}")
    Call<User> getUserById(@Path("login") String login, @Header("Authorization") String token);

    @GET("users")
    Call<List<User>> getAllUsers(@Header("Authorization") String token);

    @POST("register")
    Call<ResponseBody> registerUser(@Header("Authorization") String token, @Body UserRegister managedUserVM);

    @POST("account/reset-password/init")
    Call<ResponseBody> requestPasswordReset(@Header("Authorization") String token, @Body String mail);

    @POST("account/reset-password/finish")
    Call<Void> requestPasswordResetFinish(@Body KeyAndPass keyAndPassword);
}
