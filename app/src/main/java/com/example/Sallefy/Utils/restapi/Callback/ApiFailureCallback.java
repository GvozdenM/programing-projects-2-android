package com.example.Sallefy.Utils.restapi.Callback;

public interface ApiFailureCallback {
    void onFailure(Throwable throwable);
}
