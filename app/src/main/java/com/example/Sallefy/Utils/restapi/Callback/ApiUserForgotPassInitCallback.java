package com.example.Sallefy.Utils.restapi.Callback;

public interface ApiUserForgotPassInitCallback extends ApiFailureCallback {
    void onSentEmail();
}
