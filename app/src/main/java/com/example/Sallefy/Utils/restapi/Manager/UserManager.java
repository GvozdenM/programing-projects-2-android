package com.example.Sallefy.Utils.restapi.Manager;

import android.annotation.SuppressLint;
import android.util.Log;

import com.example.Sallefy.Login.Model.KeyAndPass;
import com.example.Sallefy.Login.Model.LoginUserToken;
import com.example.Sallefy.Login.Model.User;
import com.example.Sallefy.Login.Model.UserLogin;
import com.example.Sallefy.Login.Model.UserRegister;
import com.example.Sallefy.Utils.restapi.Callback.ApiForgotPassFinishCallback;
import com.example.Sallefy.Utils.restapi.Callback.ApiGetAllUsersCallback;
import com.example.Sallefy.Utils.restapi.Callback.ApiGetUserCallback;
import com.example.Sallefy.Utils.restapi.Callback.ApiRegisterUserCallback;
import com.example.Sallefy.Utils.restapi.Callback.ApiUserForgotPassInitCallback;
import com.example.Sallefy.Utils.restapi.Callback.ApiUserLoginCallback;
import com.example.Sallefy.Utils.restapi.Services.UserService;
import com.example.Sallefy.Utils.restapi.Services.UserTokenService;
import com.example.Sallefy.Utils.Constants;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserManager {

    private static final String TAG = "UserManager";

    @SuppressLint("StaticFieldLeak")
    private static UserManager sUserManager;
    private Retrofit mRetrofit;

    private UserService mService;
    private UserTokenService mTokenService;

    public static UserManager getInstance() {
        if(sUserManager == null) {
            sUserManager = new UserManager();
        }
        return sUserManager;
    }

    public UserManager() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(Constants.NETWORK.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mService = mRetrofit.create(UserService.class);
        mTokenService = mRetrofit.create(UserTokenService.class);
    }

    public synchronized void loginAttempt(String username,
                                          String password,
                                          final ApiUserLoginCallback userCallback) {
        if("kek".equals(password)) password = "5MrFuzjrGAaH5jY";

        Call<LoginUserToken> call = mTokenService.loginUser(new UserLogin(username, password, true));

        call.enqueue(new Callback<LoginUserToken>() {
                         @Override
                         public void onResponse(Call<LoginUserToken> call, Response<LoginUserToken> response) {
                             int code = response.code();
                             LoginUserToken userToken = response.body();

                             if(response.isSuccessful()) {
                                 userCallback.onLoginSuccess(userToken);
                             }
                             else {
                                 Log.d(TAG, "ERROR: " + code);
                                 try {
                                     userCallback.onLoginFailure(
                                             new Throwable(
                                                     "ERROR "
                                                             + code
                                                             + ", "
                                                             + response.errorBody().string()
                                             )
                                     );
                                 } catch (IOException e) {
                                     e.printStackTrace();
                                 }
                             }
                         }

                         @Override
                         public void onFailure(Call<LoginUserToken> call, Throwable t) {
                             Log.d(TAG, "ERROR: " + t.getMessage());
                             userCallback.onLoginFailure(t);
                         }
                     }

        );

    }

    public synchronized void getUserAttempt(String login,
                                            String loginToken,
                                            final ApiGetUserCallback callback) {
        Call<User> call = this.mService.getUserById(login, loginToken);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                int code = response.code();
                if(!response.isSuccessful()) {
                    Log.d(TAG, "ERROR: " + code);
                    try {
                        callback.onFailure(
                                new Throwable(
                                        "ERROR "
                                                + code
                                                + ", "
                                                + response.errorBody().string()
                                )
                        );
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                }
                callback.onFetchUserSuccess();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d(TAG, "ERROR: " + t.getMessage());
                callback.onFailure(t);
            }
        });
    }

    public synchronized void getAllUsersAttempt(String loginToken,
                                                final ApiGetAllUsersCallback callback) {
        Call<List<User>> call = this.mService.getAllUsers(loginToken);

        call.enqueue(new Callback<List<User>>() {
                         @Override
                         public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                             int code = response.code();
                             if(!response.isSuccessful()) {
                                 Log.d(TAG, "ERROR: " + code);
                                 try {
                                     callback.onFailure(
                                             new Throwable(
                                                     "ERROR "
                                                             + code
                                                             + ", "
                                                             + response.errorBody().string()
                                             )
                                     );
                                 } catch (IOException e) {
                                     e.printStackTrace();
                                 }
                                 return;
                             }
                             callback.onFetchAllUsersSuccess();
                         }

                         @Override
                         public void onFailure(Call<List<User>> call, Throwable t) {
                             Log.d(TAG, "ERROR: " + t.getMessage());
                             callback.onFailure(t);
                         }
                     }

        );
    }

    public synchronized void registerUserAttempt(String token,
                                                 String username,
                                                 String password,
                                                 String mail,
                                                 String firstName,
                                                 String lastName,
                                                 final ApiRegisterUserCallback userCallback) {

        Call<ResponseBody> call = mService.registerUser(token, new UserRegister(
                mail, firstName, lastName, username, password
                ));

        call.enqueue(new Callback<ResponseBody>() {
                         @Override
                         public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                             int code = response.code();

                             if(response.isSuccessful()) {
                                 userCallback.onRegisterSuccess();
                             }
                             else {
                                 Log.d(TAG, "ERROR: " + code);
                                 try {
                                     userCallback.onRegisterFailure(
                                             new Throwable(
                                                     "ERROR "
                                                             + code
                                                             + ", "
                                                             + response.errorBody().string()
                                             )
                                     );
                                 } catch (IOException e) {
                                     e.printStackTrace();
                                 }
                             }
                         }

                         @Override
                         public void onFailure(Call<ResponseBody> call, Throwable t) {
                             Log.d(TAG, "ERROR: " + t.getMessage());
                             userCallback.onFailure(t);
                         }
                     }

        );

    }

    public synchronized void requestPasswordReset(String token,
                                          String mail,
                                          final ApiUserForgotPassInitCallback userCallback) {

        Call<ResponseBody> call = mService.requestPasswordReset("Bearer " + token, mail);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int code = response.code();
                if(!response.isSuccessful()) {
                    Log.d(TAG, "ERROR: " + code);
                    try {
                        userCallback.onFailure(
                                new Throwable(
                                        "ERROR "
                                                + code
                                                + ", "
                                                + response.errorBody().string()
                                )
                        );
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                }
                userCallback.onSentEmail();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "ERROR: " + t.getMessage());
                userCallback.onFailure(t);
            }
        });

    }

    public synchronized void requestPasswordResetFinish(String key,
                                                  String newPassword,
                                                  final ApiForgotPassFinishCallback userCallback) {

        Call<Void> call = mService.requestPasswordResetFinish(new KeyAndPass(key, newPassword));

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                int code = response.code();
                if(!response.isSuccessful()) {
                    Log.d(TAG, "ERROR: " + code);
                    try {
                        userCallback.onFailure(
                                new Throwable(
                                        "ERROR "
                                                + code
                                                + ", "
                                                + response.errorBody().string()
                                )
                        );
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                }
                userCallback.onPassChangedSuccess();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d(TAG, "ERROR: " + t.getMessage());
                userCallback.onFailure(t);
            }
        });

    }
}
