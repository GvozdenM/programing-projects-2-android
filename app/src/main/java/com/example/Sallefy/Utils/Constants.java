package com.example.Sallefy.Utils;

public class Constants {
    public static final String URL = "url";
    public interface NETWORK {
        public static String BASE_URL = "http://" + "sallefy.eu-west-3.elasticbeanstalk.com/api/";
    }

    public interface LOGIN_STATE {
        public static String back = "back";
        public static String login = "login";
        public static String register = "register";
        public static String registerSuccess = "registerSuccess";
        public static String forgotPassword = "forgotPassword";
        public static String authenticated = "authenticated";

    }
}
