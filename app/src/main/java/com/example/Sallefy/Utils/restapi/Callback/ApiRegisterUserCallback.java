package com.example.Sallefy.Utils.restapi.Callback;

public interface ApiRegisterUserCallback extends ApiFailureCallback {
    void onRegisterSuccess();
    void onRegisterFailure(Throwable throwable);
}
