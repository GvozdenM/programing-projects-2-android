package com.example.Sallefy.Utils.restapi.Callback;

import com.example.Sallefy.Login.Model.LoginUserToken;
import com.example.Sallefy.Login.Model.User;

public interface ApiUserLoginCallback {
    void onLoginSuccess(LoginUserToken userToken);
    void onLoginFailure(Throwable throwable);
}
