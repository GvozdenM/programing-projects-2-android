package com.example.Sallefy.Utils.restapi.Callback;

public interface ApiForgotPassFinishCallback extends ApiFailureCallback {
    void onPassChangedSuccess();
}
