package com.example.Sallefy.Utils;

public class Promise extends AbstractPromise {
    public Promise(RejectCallback rejectCallback,
                   AcceptCallback acceptCallback) {
        super(acceptCallback, rejectCallback);
    }

    public Promise() {}

    @Override
    public void accept() {
        this.onSuccess.accept();
    }

    @Override
    public void reject() {
        this.onFail.reject();
    }
}
