package com.example.Sallefy.Utils;

@FunctionalInterface
public interface AcceptCallback {
    void accept();
}
