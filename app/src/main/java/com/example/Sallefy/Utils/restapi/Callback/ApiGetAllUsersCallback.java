package com.example.Sallefy.Utils.restapi.Callback;

public interface ApiGetAllUsersCallback extends ApiFailureCallback{
    void onFetchAllUsersSuccess();
}
