package com.example.Sallefy.Utils.restapi.Callback;

public interface ApiGetUserCallback extends ApiFailureCallback {
    void onFetchUserSuccess();
}
