package com.example.Sallefy.Utils;

@FunctionalInterface
public interface AcceptConsumerCallback<T> {
    void accept(T data);
}
