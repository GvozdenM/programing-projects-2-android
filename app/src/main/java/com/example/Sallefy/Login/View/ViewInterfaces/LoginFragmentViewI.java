package com.example.Sallefy.Login.View.ViewInterfaces;

import com.example.Sallefy.Login.Controller.ListenerInterfaces.LoginListenerI;

public interface LoginFragmentViewI extends ViewInterface {
    void attachLoginButtonListener(LoginListenerI listener);
    void attachRegisterButtonListener(LoginListenerI listener);
    void attachForgotPassListener(LoginListenerI listener);
    void displayWrongLogin();
    void displayLoginLock();
    void displayLoginUnlock();
    String getUsernameString();
    String getPasswordString();
}
