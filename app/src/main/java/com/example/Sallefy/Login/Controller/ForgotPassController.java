package com.example.Sallefy.Login.Controller;

import com.example.Sallefy.Login.Model.LoginModel;
import com.example.Sallefy.Login.Model.LoginUserToken;
import com.example.Sallefy.Login.Repository.ForgotPassRepository;
import com.example.Sallefy.Login.View.ViewInterfaces.ForgotPasswordFragmentViewI;
import com.example.Sallefy.Utils.Constants;

public class ForgotPassController implements ForgotPassControllerI {
    private LoginModel model;
    private ForgotPasswordFragmentViewI view;
    private ForgotPassRepository repository;

    private String state;

    public ForgotPassController(LoginModel model, ForgotPasswordFragmentViewI view) {
        this.state = "init";
        this.model = model;
        this.view = view;
        this.repository = new ForgotPassRepository();
        attachListeners();
    }

    private void attachListeners() {
        switch (this.state) {
            case "init":
                this.view.attachBackButtonListener(l -> processBackButtonClick());
                this.view.attachConfirmButtonListener(l -> processSendMailClick());
                break;

            case "confirmation":
                this.view.attachConfirmButtonListener(l -> processConfirmKeyClick());
        }
    }

    private void processSendMailClick() {
        this.repository.queryTmpToken(this);
        this.view.displayButtonLock();
    }

    private void processConfirmKeyClick() {
        this.model.setPassResetKey(this.view.getFormContent());
    }

    private void processBackButtonClick() {
        this.model.setState(Constants.LOGIN_STATE.back);
    }

    @Override
    public void onSentEmail() {
        this.view.displayButtonUnlock();
        this.view.pivotView();
    }

    @Override
    public void onFailure(Throwable throwable) {
        this.view.displayWrongInput();
        this.view.displayButtonUnlock();
    }

    @Override
    public void onPassChangedSuccess() {
        //TODO
    }

    @Override
    public void onLoginSuccess(LoginUserToken userToken) {
        this.repository.queryPasswordResetInit(userToken.getIdToken(), this.view.getFormContent(), this);
    }

    @Override
    public void onLoginFailure(Throwable throwable) {

    }
}
