package com.example.Sallefy.Login.View.ViewInterfaces;

import com.example.Sallefy.Login.Controller.ListenerInterfaces.LoginForgotPassListener;

public interface ForgotPasswordFragmentViewI extends ViewInterface {
    void attachBackButtonListener(LoginForgotPassListener listener);
    void attachConfirmButtonListener(LoginForgotPassListener listener);
    String getFormContent();
    void displayButtonLock();
    void displayButtonUnlock();
    void pivotView();
    void displayWrongInput();
}
