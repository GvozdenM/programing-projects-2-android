package com.example.Sallefy.Login.Controller.Fragment;

import com.example.Sallefy.Login.Model.LoginModel;

public class LoginFragmentFactory implements LoginFragmentFactoryI {
    private LoginModel loginModel;

    public LoginFragmentFactory(LoginModel loginModel) {
        this.loginModel = loginModel;
    }

    @Override
    public LoginFragment createLoginFragment() {
        return new LoginFragment(this.loginModel);
    }

    @Override
    public AbstractLoginFragment createForgotPasswordFragment() {
        return new ForgotPasswordFragment(this.loginModel);
    }

    @Override
    public AbstractLoginFragment createRegisterFragment() {
        return new RegisterFragment(this.loginModel);
    }

    @Override
    public AbstractLoginFragment createRegisterSuccessFragment() {
        return new RegisterSuccessFragment(this.loginModel);
    }
}
