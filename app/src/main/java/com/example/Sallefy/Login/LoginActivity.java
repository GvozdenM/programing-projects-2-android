package com.example.Sallefy.Login;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.Sallefy.Login.Controller.Fragment.LoginFragmentFactory;
import com.example.Sallefy.Login.Model.LoginFragmentHolder;
import com.example.Sallefy.Login.Model.LoginModel;
import com.example.Sallefy.Login.View.LoginActivityView;
import com.example.Sallefy.Login.View.ViewInterfaces.LoginActivityViewI;
import com.example.Sallefy.MainActivity;
import com.example.Sallefy.R;
import com.example.Sallefy.Utils.Constants;

public class LoginActivity extends AppCompatActivity {
    private LoginModel model;
    private LoginActivityViewI view;
    private LoginFragmentHolder fragmentHolder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_main);
        this.model = new LoginModel();
        this.fragmentHolder = new LoginFragmentHolder(
                new LoginFragmentFactory(this.model)
        );

        this.view = new LoginActivityView(
                getLayoutInflater(),
                findViewById(R.id.login_container),
                getSupportFragmentManager()
        );
        attachFragmentStateObserver();

        this.model.setState("login");
    }

    private void attachFragmentStateObserver() {
        this.model.getState().observe(
                this,
                this::evaluateState
                );
    }

    private void evaluateState(String state) {
        switch (state) {
            case Constants.LOGIN_STATE.login:
                this.view.transition(this.fragmentHolder.getNewLoginFragment());
                break;

            case Constants.LOGIN_STATE.forgotPassword:
                this.view.transition(this.fragmentHolder.getNewForgotPassFragment());
                break;

            case Constants.LOGIN_STATE.register:
                this.view.transition(this.fragmentHolder.getNewRegisterFragment());
                break;

            case Constants.LOGIN_STATE.registerSuccess:
                this.view.transition(this.fragmentHolder.getNewRegisterSuccessFragment());
                break;

            case Constants.LOGIN_STATE.back:
                this.view.transitionBack();
                break;

            case Constants.LOGIN_STATE.authenticated:
                this.model.setAuthenticated(true);
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                Bundle b = new Bundle();
                //TODO wrap inside a response class
                b.putBoolean("authenticated", this.model.isAuthenticated());
                b.putString("userID", this.model.getUserID());
                b.putString("loginToken", this.model.getLoginToken());
                intent.putExtras(b);
                startActivity(intent);
                this.finish();
                break;
        }
    }
}
