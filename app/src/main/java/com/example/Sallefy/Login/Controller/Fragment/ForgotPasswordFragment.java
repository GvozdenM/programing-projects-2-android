package com.example.Sallefy.Login.Controller.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.Sallefy.Login.Controller.ForgotPassController;
import com.example.Sallefy.Login.Controller.ForgotPassControllerI;
import com.example.Sallefy.Login.Model.LoginModel;
import com.example.Sallefy.Login.View.ForgotPassFragmentView;
import com.example.Sallefy.Login.View.ViewInterfaces.ForgotPasswordFragmentViewI;

public class ForgotPasswordFragment extends AbstractLoginFragment {
    private ForgotPasswordFragmentViewI view;
    private ForgotPassControllerI controller;


    public ForgotPasswordFragment(LoginModel loginModel) {
        super(loginModel);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = new ForgotPassFragmentView(inflater, container);
        this.controller = new ForgotPassController(this.loginModel, this.view);
        return view.getRootView();
    }
}
