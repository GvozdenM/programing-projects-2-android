package com.example.Sallefy.Login.Repository;

import com.example.Sallefy.Login.Repository.Callback.UserForgotPassCallback;
import com.example.Sallefy.Login.Repository.Callback.UserForgotPassFinishCallback;
import com.example.Sallefy.Utils.restapi.Manager.UserManager;

public class ForgotPassRepository extends LoginRepository{
    public void queryPasswordResetInit(String token, String mail, UserForgotPassCallback callback) {
        UserManager.getInstance().requestPasswordReset(token, mail, callback);
    }

    public void queryPasswordResetFinish(String key, String newPassword, UserForgotPassFinishCallback callback) {
        UserManager.getInstance().requestPasswordResetFinish(key, newPassword, callback);
    }
}
