package com.example.Sallefy.Login.View.ViewInterfaces;

import com.example.Sallefy.Login.Controller.ListenerInterfaces.RegisterListenerI;

public interface RegisterFragmentViewI extends ViewInterface {
    void attachSignUpButtonListener(RegisterListenerI listener);
    void attachBackButtonListener(RegisterListenerI listener);
    void displayPasswordMismatch();
    void displayUsernameExists();
    String getUsername();
    String getEmail();
    String getPasswordOne();
    String getPasswordTwo();
    String getFirstNameForm();
    String getLastNameForm();
}
