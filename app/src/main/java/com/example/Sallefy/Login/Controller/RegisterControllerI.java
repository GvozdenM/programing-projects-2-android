package com.example.Sallefy.Login.Controller;

import com.example.Sallefy.Login.Repository.Callback.RegisterUserCallback;
import com.example.Sallefy.Login.Repository.Callback.UserLoginCallback;

public interface RegisterControllerI extends RegisterUserCallback, UserLoginCallback {

}
