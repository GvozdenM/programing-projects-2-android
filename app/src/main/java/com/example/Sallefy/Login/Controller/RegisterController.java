package com.example.Sallefy.Login.Controller;

import com.example.Sallefy.Login.Model.LoginModel;
import com.example.Sallefy.Login.Model.LoginUserToken;
import com.example.Sallefy.Login.Repository.RegisterRepository;
import com.example.Sallefy.Login.View.ViewInterfaces.RegisterFragmentViewI;
import com.example.Sallefy.Utils.Constants;

public class RegisterController implements RegisterControllerI {
    private LoginModel model;
    private RegisterRepository repository;
    private RegisterFragmentViewI view;

    public RegisterController(LoginModel model, RegisterFragmentViewI view) {
        this.model = model;
        this.view = view;
        this.repository = new RegisterRepository();
        attachListeners();
    }

    private void attachListeners() {
        this.view.attachBackButtonListener(l -> backButtonClick());
        this.view.attachSignUpButtonListener(l -> signupButtonClick());
    }


    private void backButtonClick() {
        model.setState(Constants.LOGIN_STATE.login);
    }

    private void signupButtonClick() {
        if(validUserCredentials()) {
            repository.queryTmpToken(this);
        }
    }

    private boolean validUserCredentials() {
        if(passwordCredCheck()) {
            return true;
        }
        this.view.displayPasswordMismatch();
        return false;
    }

    private boolean passwordCredCheck() {
        return this.view.getPasswordOne().length() >= 4
                && this.view.getPasswordOne().equals(this.view.getPasswordTwo());
    }

    @Override
    public void onRegisterSuccess() {
        model.setState(Constants.LOGIN_STATE.registerSuccess);
    }

    @Override
    public void onRegisterFailure(Throwable throwable) {
        this.view.displayUsernameExists();
    }

    @Override
    public void onFailure(Throwable throwable) {

    }

    @Override
    public void onLoginSuccess(LoginUserToken userToken) {
        repository.queryUserRegistration(userToken.getIdToken(),
                this,
                this.view.getFirstNameForm(),
                this.view.getLastNameForm(),
                this.view.getUsername(),
                this.view.getPasswordOne(),
                this.view.getEmail()
        );
    }

    @Override
    public void onLoginFailure(Throwable throwable) {

    }
}
