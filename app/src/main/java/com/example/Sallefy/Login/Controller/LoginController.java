package com.example.Sallefy.Login.Controller;

import com.example.Sallefy.Login.Model.LoginModel;
import com.example.Sallefy.Login.Model.LoginUserToken;
import com.example.Sallefy.Login.Repository.TokenRepository;
import com.example.Sallefy.Login.View.ViewInterfaces.LoginFragmentViewI;
import com.example.Sallefy.Utils.Constants;

public class LoginController implements LoginControllerI {
    private LoginModel model;
    private LoginFragmentViewI view;
    private TokenRepository repository;


    public LoginController(LoginModel model, LoginFragmentViewI view) {
        this.model = model;
        this.view = view;
        this.repository = new TokenRepository();
        attachListeners();
    }

    private void attachListeners() {
        this.view.attachLoginButtonListener(l -> loginButtonClick());
        this.view.attachRegisterButtonListener(l -> registerButtonClick());
        this.view.attachForgotPassListener(l -> forgotPassButtonClick());
    }

    private void loginButtonClick() {
        login(this.view.getUsernameString(), this.view.getPasswordString());
        this.view.displayLoginLock();
    }

    private void registerButtonClick() {
        this.model.setState(Constants.LOGIN_STATE.register);
    }

    private void forgotPassButtonClick() {
        this.model.setState(Constants.LOGIN_STATE.forgotPassword);
    }

    private void login(String username, String password) {
        this.repository.queryUserLogin(username, password, this);
    }

    @Override
    public void onLoginSuccess(LoginUserToken userToken) {
        this.model.setUserID(this.repository.getQueriedUserLogin());
        this.model.setLoginToken(userToken.getIdToken());
        model.setAuthenticated(true);
        this.model.setState(Constants.LOGIN_STATE.authenticated);
    }

    @Override
    public void onLoginFailure(Throwable throwable) {
        this.view.displayLoginUnlock();
        this.view.displayWrongLogin();
    }
}
