package com.example.Sallefy.Login.Model;

import com.example.Sallefy.Login.Controller.Fragment.AbstractLoginFragment;
import com.example.Sallefy.Login.Controller.Fragment.LoginFragmentFactoryI;
import com.example.Sallefy.Login.Controller.Fragment.LoginFragment;

public class LoginFragmentHolder {
    private LoginFragmentFactoryI loginFragmentFactory;

    private LoginFragment loginFragment;
    private AbstractLoginFragment registerFragment;
    private AbstractLoginFragment registerSuccessFragment;

    private AbstractLoginFragment forgotPassFragment;


    public LoginFragmentHolder(LoginFragmentFactoryI loginFragmentFactory) {
        this.loginFragmentFactory = loginFragmentFactory;
    }

    public AbstractLoginFragment getRegisterSuccessFragment() {
        if(null == this.loginFragment) {
            this.registerSuccessFragment = this.loginFragmentFactory.createRegisterSuccessFragment();
        }
        return registerSuccessFragment;
    }
    public AbstractLoginFragment getNewRegisterSuccessFragment() {
        this.registerSuccessFragment = this.loginFragmentFactory.createRegisterSuccessFragment();
        return registerSuccessFragment;
    }

    public LoginFragment getLoginFragment() {
        if(null == this.loginFragment) {
            this.loginFragment = this.loginFragmentFactory.createLoginFragment();
        }
        return loginFragment;
    }

    public AbstractLoginFragment getRegisterFragment() {
        if(null == this.registerFragment) {
            this.registerFragment = this.loginFragmentFactory.createRegisterFragment();
        }
        return registerFragment;
    }

    public AbstractLoginFragment getForgotPassFragment() {
        if(null == this.forgotPassFragment) {
            this.forgotPassFragment = this.loginFragmentFactory.createForgotPasswordFragment();
        }
        return forgotPassFragment;
    }

    public LoginFragment getNewLoginFragment() {
        this.loginFragment = this.loginFragmentFactory.createLoginFragment();
        return loginFragment;
    }

    public AbstractLoginFragment getNewRegisterFragment() {
        this.registerFragment = this.loginFragmentFactory.createRegisterFragment();
        return registerFragment;
    }

    public AbstractLoginFragment getNewForgotPassFragment() {
        this.forgotPassFragment = this.loginFragmentFactory.createForgotPasswordFragment();
        return forgotPassFragment;
    }

}
