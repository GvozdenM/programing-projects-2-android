package com.example.Sallefy.Login.Controller.Fragment;

public interface LoginFragmentFactoryI {
    LoginFragment createLoginFragment();
    AbstractLoginFragment createForgotPasswordFragment();
    AbstractLoginFragment createRegisterFragment();
    AbstractLoginFragment createRegisterSuccessFragment();
}
