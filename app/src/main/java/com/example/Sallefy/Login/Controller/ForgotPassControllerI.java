package com.example.Sallefy.Login.Controller;

import com.example.Sallefy.Login.Repository.Callback.UserForgotPassCallback;
import com.example.Sallefy.Login.Repository.Callback.UserForgotPassFinishCallback;
import com.example.Sallefy.Login.Repository.Callback.UserLoginCallback;

public interface ForgotPassControllerI extends
        UserForgotPassCallback,
        UserForgotPassFinishCallback,
        UserLoginCallback {

}
