package com.example.Sallefy.Login.View.ViewInterfaces;

import androidx.fragment.app.Fragment;

public interface LoginActivityViewI extends ViewInterface {

    void transition(Fragment dest);

    void transitionBack();

}
