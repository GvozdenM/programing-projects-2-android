package com.example.Sallefy.Login.Controller.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.Sallefy.Login.Controller.RegisterController;
import com.example.Sallefy.Login.Controller.RegisterControllerI;
import com.example.Sallefy.Login.Model.LoginModel;
import com.example.Sallefy.Login.View.ViewInterfaces.RegisterFragmentViewI;
import com.example.Sallefy.Login.View.RegisterFragmentView;

public class RegisterFragment extends AbstractLoginFragment {
    private RegisterFragmentViewI view;
    private RegisterControllerI controller;

    public RegisterFragment(LoginModel loginModel) {
        super(loginModel);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = new RegisterFragmentView(inflater, container);
        this.controller = new RegisterController(this.loginModel, this.view);

        return this.view.getRootView();
    }

}
