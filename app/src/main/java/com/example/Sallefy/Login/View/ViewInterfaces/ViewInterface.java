package com.example.Sallefy.Login.View.ViewInterfaces;

import android.os.Bundle;
import android.view.View;

public interface ViewInterface {
    View getRootView();

    Bundle getViewState();
}
