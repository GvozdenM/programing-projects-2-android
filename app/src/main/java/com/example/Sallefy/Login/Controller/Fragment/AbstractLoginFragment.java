package com.example.Sallefy.Login.Controller.Fragment;

import androidx.fragment.app.Fragment;

import com.example.Sallefy.Login.Model.LoginModel;

public abstract class AbstractLoginFragment extends Fragment {
    protected LoginModel loginModel;

    public AbstractLoginFragment(LoginModel loginModel) {
        this.loginModel = loginModel;
    }
}
