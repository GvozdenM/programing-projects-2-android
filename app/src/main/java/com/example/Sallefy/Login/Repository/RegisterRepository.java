package com.example.Sallefy.Login.Repository;

import com.example.Sallefy.Login.Repository.Callback.RegisterUserCallback;
import com.example.Sallefy.Utils.restapi.Manager.UserManager;

public class RegisterRepository extends LoginRepository {
    public void queryUserRegistration(String token,
                               RegisterUserCallback callback,
                               String firstName,
                               String lastName,
                               String login,
                               String password,
                               String email
    ) {
        UserManager.getInstance().registerUserAttempt(token, login, password, email, firstName, lastName, callback);
    }
}
