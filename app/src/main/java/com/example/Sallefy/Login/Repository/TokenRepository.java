package com.example.Sallefy.Login.Repository;

import com.example.Sallefy.Login.Repository.Callback.UserLoginCallback;
import com.example.Sallefy.Utils.restapi.Manager.UserManager;

public class TokenRepository {
    private String userLogin;

    public void queryUserLogin(String username, String password, UserLoginCallback callback) {
        UserManager.getInstance().loginAttempt(username, password, callback);
    }

    public String getQueriedUserLogin() {
        return userLogin;
    }
}
