package com.example.Sallefy.Login.View.ViewInterfaces;

import com.example.Sallefy.Login.Controller.ListenerInterfaces.RegisterSuccessListenerI;

public interface RegisterSuccessFragViewI extends ViewInterface {
    void attachLoginButtonListener(RegisterSuccessListenerI listener);
}
