package com.example.Sallefy.Login.Controller.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.Sallefy.Login.Controller.LoginController;
import com.example.Sallefy.Login.Controller.LoginControllerI;
import com.example.Sallefy.Login.Model.LoginModel;
import com.example.Sallefy.Login.View.LoginFragmentView;
import com.example.Sallefy.Login.View.ViewInterfaces.LoginFragmentViewI;


public class LoginFragment extends AbstractLoginFragment {
    private LoginFragmentViewI view;
    private LoginControllerI controller;

    public LoginFragment(LoginModel loginModel) {
        super(loginModel);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = new LoginFragmentView(inflater, container);
        controller = new LoginController(this.loginModel, this.view);

        return this.view.getRootView();
    }
}
