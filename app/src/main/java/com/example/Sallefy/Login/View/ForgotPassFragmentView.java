package com.example.Sallefy.Login.View;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.Sallefy.Login.Controller.ListenerInterfaces.LoginForgotPassListener;
import com.example.Sallefy.Login.View.ViewInterfaces.ForgotPasswordFragmentViewI;
import com.example.Sallefy.R;

public class ForgotPassFragmentView implements ForgotPasswordFragmentViewI, TextWatcher {
    private View rootView;
    private ImageButton backButton;

    private EditText inputForm;
    private Button confirmButton;

    public ForgotPassFragmentView(LayoutInflater inflater, ViewGroup container) {
        this.rootView = inflater.inflate(R.layout.login_fragment_forgot_password,
                container, false);

        findViewElements();
        attachTextChangeListeners();
        processSendButtonStatus();
    }

    private void findViewElements() {
        this.backButton = this.rootView.findViewById(R.id.login_forgot_password_button_back);
        this.confirmButton = this.rootView.findViewById(R.id.login_forgot_password_button_send);
        this.inputForm = this.rootView.findViewById(R.id.login_forgot_password_form);
    }

    private void processSendButtonStatus() {
        if("".equals(inputForm.getText().toString())) {
            confirmButton.setClickable(false);
            confirmButton.setAlpha(.2f);
            confirmButton.setBackgroundResource(R.drawable.rounded_button_untoggled);
        }
        else {
            confirmButton.setAlpha(0.9f);
            confirmButton.setBackgroundResource(R.drawable.rounded_button);
            confirmButton.setClickable(true);
        }
    }

    private void attachTextChangeListeners() {
        this.inputForm.addTextChangedListener(this);
    }

    public String getInputForm() {
        return inputForm.getText().toString();
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        processSendButtonStatus();
    }

    @Override
    public void attachBackButtonListener(LoginForgotPassListener listener) {
        this.backButton.setOnClickListener(listener);
    }

    @Override
    public void attachConfirmButtonListener(LoginForgotPassListener listener) {
        confirmButton.setOnClickListener(listener);
    }

    @Override
    public String getFormContent() {
        return this.inputForm.getText().toString();
    }

    @Override
    public void displayButtonLock() {
        this.inputForm.setClickable(false);

        this.confirmButton.setAlpha(0.5f);
        this.confirmButton.setClickable(false);
    }

    @Override
    public void displayButtonUnlock() {
        this.inputForm.setClickable(true);
        this.confirmButton.setClickable(true);
        processSendButtonStatus();
    }

    /**
     * @Objective: To transform the view from receiving a email input to receiving
     *              a verification key input.
     */
    @Override
    public void pivotView() {
        this.inputForm.setHint("Verification key");
        this.confirmButton.setText(R.string.login_forgot_password_confirm_key);
        Animation shake = AnimationUtils.loadAnimation(this.rootView.getContext(), R.anim.shake_edittext);
        this.inputForm.startAnimation(shake);
        this.inputForm.setBackgroundResource(R.drawable.rounded_edittext_green_border);
    }

    @Override
    public void displayWrongInput() {
        Animation shake = AnimationUtils.loadAnimation(this.rootView.getContext(), R.anim.shake_edittext);
        this.inputForm.startAnimation(shake);
        this.inputForm.setBackgroundResource(R.drawable.rounded_edittext_red_border);
    }
}
