package com.example.Sallefy.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class HomeViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public HomeViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Facilisis leo vel fringilla est ullamcorper eget nulla facilisi etiam. Egestas dui id ornare arcu odio. Vel eros donec ac odio tempor orci dapibus ultrices. Diam sit amet nisl suscipit adipiscing bibendum. Tempor nec feugiat nisl pretium fusce id velit ut tortor. Tellus elementum sagittis vitae et leo duis ut. Diam phasellus vestibulum lorem sed risus ultricies tristique nulla. Vel risus commodo viverra maecenas accumsan lacus vel. Faucibus vitae aliquet nec ullamcorper sit amet risus. Elit ullamcorper dignissim cras tincidunt lobortis feugiat vivamus at augue. Quam lacus suspendisse faucibus interdum posuere lorem ipsum.\n" +
                "\n" +
                "Vel pretium lectus quam id leo in. Turpis tincidunt id aliquet risus feugiat in. Volutpat commodo sed egestas egestas fringilla phasellus. Imperdiet dui accumsan sit amet nulla facilisi morbi tempus iaculis. Diam volutpat commodo sed egestas egestas fringilla phasellus faucibus. Nulla facilisi nullam vehicula ipsum a arcu cursus. Lacus sed viverra tellus in hac habitasse platea. Enim praesent elementum facilisis leo vel fringilla est ullamcorper eget. Sed augue lacus viverra vitae congue eu consequat ac felis. Cursus euismod quis viverra nibh cras. Amet justo donec enim diam vulputate. Blandit massa enim nec dui nunc mattis enim. Quam lacus suspendisse faucibus interdum posuere. Urna nunc id cursus metus aliquam. Mauris augue neque gravida in. Tincidunt praesent semper feugiat nibh sed pulvinar. Erat velit scelerisque in dictum non consectetur a. Porta non pulvinar neque laoreet suspendisse. Velit egestas dui id ornare arcu odio.\n" +
                "\n" +
                "Rhoncus dolor purus non enim. Interdum consectetur libero id faucibus nisl tincidunt. Volutpat sed cras ornare arcu dui vivamus arcu. Nunc mi ipsum faucibus vitae. Vitae congue eu consequat ac felis. Tortor at auctor urna nunc. Nibh tellus molestie nunc non blandit massa enim. Dui vivamus arcu felis bibendum. Fusce id velit ut tortor pretium viverra suspendisse potenti nullam. Quis enim lobortis scelerisque fermentum dui faucibus. Etiam erat velit scelerisque in dictum non. Feugiat sed lectus vestibulum mattis. Aliquet lectus proin nibh nisl condimentum id venenatis. Feugiat pretium nibh ipsum consequat nisl vel pretium lectus. Diam vel quam elementum pulvinar. Molestie ac feugiat sed lectus. Faucibus a pellentesque sit amet porttitor eget dolor. Mauris pharetra et ultrices neque ornare aenean. Erat imperdiet sed euismod nisi porta. Tellus molestie nunc non blandit massa enim nec.\n" +
                "\n" +
                "Eget nulla facilisi etiam dignissim diam quis enim. Aliquam etiam erat velit scelerisque in dictum non. Turpis nunc eget lorem dolor. Lacus sed turpis tincidunt id. Egestas tellus rutrum tellus pellentesque eu tincidunt tortor aliquam. Et netus et malesuada fames ac turpis egestas. Fames ac turpis egestas sed tempus urna et pharetra. In cursus turpis massa tincidunt dui ut. Aenean sed adipiscing diam donec adipiscing tristique risus nec feugiat. Viverra justo nec ultrices dui sapien eget mi. Eleifend donec pretium vulputate sapien nec. Eget mauris pharetra et ultrices. Risus ultricies tristique nulla aliquet enim tortor. Nascetur ridiculus mus mauris vitae ultricies leo integer. Consectetur libero id faucibus nisl tincidunt eget nullam. Nullam ac tortor vitae purus. Senectus et netus et malesuada fames.\n" +
                "\n" +
                "Vehicula ipsum a arcu cursus vitae congue mauris rhoncus. Dignissim suspendisse in est ante in nibh mauris. Ut etiam sit amet nisl purus. Commodo viverra maecenas accumsan lacus vel. Habitasse platea dictumst vestibulum rhoncus est pellentesque. Porttitor massa id neque aliquam vestibulum morbi blandit. Tincidunt arcu non sodales neque sodales ut etiam sit amet. Consectetur lorem donec massa sapien faucibus et molestie. Volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque in. Sapien et ligula ullamcorper malesuada. Malesuada proin libero nunc consequat interdum varius sit amet mattis. Amet mauris commodo quis imperdiet massa tincidunt.");
    }

    public LiveData<String> getText() {
        return mText;
    }
}